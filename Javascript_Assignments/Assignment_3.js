url = "https://demo1451655.mockable.io/test2";

//with call back
// here we will pass a function when calling another  so that the function callback will be implemented
function getdat_with_callback(call_bk){
    return fetch(url).then((respp)=>{
        return respp.json(); //returning jon format 
    }).then((data)=>{
        console.log("with callback data",data);
        call_bk(data);
    });
}
let call_back = function (da){
    str3 ="";
    Object.keys(da).forEach((key)=>{
        str3+= `${key} = ${da[key]} <br>`;
        document.getElementById("dem1").innerHTML= str3;
    });
}
getdat_with_callback(call_back);



// with promises:

function getdata(){
    fetch(url).then((resp)=>{
        return resp.json();
    }).then((data)=>{
        console.log(data);
        strr="";
        Object.keys(data).forEach((key)=>{
            strr+= `${key} = ${data[key]} <br>`;
            document.getElementById("demm").innerHTML = strr;

        });
    });

}
getdata();

// with await:

async function get_with_await(){
    const rep1 = await fetch(url); // this will execute first after executing the statement outside the async function 
    const dat = await rep1.json(); // before this it will check whether it has some code left to complete outside function and then if not it executes
    // console.log(dat);
    str2 = "";
    Object.keys(dat).forEach((key)=>{
        str2+= `${key} = ${dat[key]} <br>`
        document.getElementById("dem2").innerHTML = str2;
    });
    return dat;

}
let an = get_with_await(); //this will return a promise while waiting for one to complete
an.then((d)=>{

    return console.log("from await: ",d);
});
// strr2="";
// Object.keys(b).forEach((key)=>{
//     str2+= `${key} = ${b[key]} <br>`;
//     document.getElementById("dem2").innerHTML = str2;

// });

