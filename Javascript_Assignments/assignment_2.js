// this is for object creation first with new keyword

var person = new Object();
person.name = "Hrithik";
person.age = 21;

console.log(person);
function createp(){
    var person = new Object();
    person.name = "Lucifer";
    person.age = 21;
    console.log(person);
}
createp();

// creating  way 2
var person2 = {};
person2.name = "Monty";
person2.age = 21;
console.log(person2);


// you can also create properties using the square braces
var person3 = {};
person3["name"] = "David";
person3["age"] = 21; //just like assinging vlues to keys
console.log(person3);

//obaject literal

var person4 = {
    name:"Hrithik_Anagire",
    age:21
}

console.log(person4);
//function type:
function person5(name,age){
    this.name = name;
    this.age=age;
}
var p = new person5("Takemaru",21);
console.log(p);


// now as we have seen creating an object with new keyword and the object literal 

// forming method in the person4 and person object 

// first with type similar to person4

var person44 = {
    name : "Hrithik",
    age:21,
    fullname: function(){
        console.log(this.name + " age is  "+ this.age);
    }
}
// function calling by the use of object as:
person44.fullname();
// ouput result: Hrithik age is  21

// now with the new keyword
var person55 = new Object();
person55.name = "Rohit";
person55.age = 22;
person55.descrip = function(){
    console.log(this.name + " age is " + this.age);
};

// calling it
person55.descrip();

// results received: Rohit age is 22

// Test para 2

// iteration with for in 

// consider person44 and another with constructor
function Personn(name,age){
    this.name = name;
    this.age = age;

}
var person66 = new Personn("Hrithik",21);
// console.log(person66);
console.log("with person44: "+ person44.name);
console.log("with personnn constuctor: "+ person66.name);


/*
now the normal methods to declare a object is these two below

method 1 : 

var person = new Object();
person.name = "Hrithik";
person.age = 21;

and :
method 2:


var person44 = {
    name : "Hrithik",
    age:21,
    fullname: function(){
        console.log(this.name + " age is  "+ this.age);
    }
}

 the disadvantages in above two methods is that 

 both the object and the methods inside them has to be repeated 
 i.e the methods that are declare in such object cannot be used by other object se
 the same code has to be re-written in the every object

 2. and when we import such object into the html file then the calling form the html
 file does not give the desired result because JAVASCRIPT DOES NOT SUPPORT FUCNTIONAL OVERLAODING INSETEAD IT FOLLOWS THE METHOD OF OVERRIDING IN THE 
 ORDER THAT THE FILE ARE INCLUDED:

 so to resolve this we declare a function and create its instances 


 function person(name,age){
    var o = new Object();
    o.name = name;
    o.age = age;
    o.sayhi = function(){
        console.log("say hii");
    }
    return o;//returning the object that we have created
 }

 var a = person(hrithik,21);
 var b = person(lcuifer,22);
 
 but this above method solved one proble of code repetition but hte calling
 confusion still prevails i.e when we call from html document the function override the function that is decalre and definned first
 so to cure that we followed the constructor object


 method 4 constructor object 

 function person(name,age,pro){
     this.name = name;
     this.age = age;
     this.sayhi = function(){
         console.log("say hi")
     }

 }

 var p = new person(hrithik,age,student);
 the problem is it create object without display and assign attributes
 and methods directly to the this object and no return statement and every time we use
 we create a new instance .

 that is this function wehn ccreated it return an empty object in the memory context and then we 
 initialize properties in it 

 the global scope. In this way, all instances can call this method. But the ensuing problem is that the methods defined in the global 
 scope are actually only called on a few instances, making the global scope a bit of a misnomer. Moreover, the object needs to define a lot of methods, 
 so it needs to define a lot of functions of the global scope, then our custom reference 
 type has no encapsulation at all. So you can use the prototype mode to solve this problem.

METHOD 4 :PROTOTYPE 
each function has a prototype property. this property is a pointer to the prototype of theobject
this prototypr object contains the properties and methods shared by the instances.
prototype is the prototyp eof th eobject of the instance created by calling the constructor

advantage : all the object instances share the methods and properties in the prototype object 
function Person() {

}

Person.prototype.name = "Jhon";
Person.prototype.age = "12";
Person.prototype.obj = "doctor";
Person.prototype.sayName = function() {
    console.log(this.name);
}

var p5 = new Person();
p5.sayName()  // "Jhon"
var p6 = new Person();
p6.sayName()  // "Jhon"

console.log(p5.sayName() === p6.sayName())  // true

next is the combo of construtor mode and prototype mode

// be fore this refer to the what prototype is what prototype chain is 
an then

https://programmerall.com/article/7766181197/


 */



