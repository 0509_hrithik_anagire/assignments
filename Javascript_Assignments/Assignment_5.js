// class Person{
    
//     walk(){
//         return "he walks faster";
//     }
// }
// class child extends Person{
//     swim(){
//         return "he can swim";
//     }

// }


// const p = new Person();
// const t = new child();
// console.log(p.walk());
// console.log(t.walk()); //bsaic concept of inheritance where child can access the parent funcion 
// console.log(t.swim());

class player{
    constructor(name,age){
        this.name = name;
        this.age = age;
    }
    pinfo(){
        console.log(`I am ${this.name} and i'm ${this.age} years old`);

    }

}
class play extends player{
    constructor(name,age,exp){
        super(name,age);
        this.exp = exp;

    }

    pro(){
        console.log(`he has ${this.exp} years of Experience`);
    }

}

// const me = player("luy",21);
const you = new play("lucy",21,4);
// me.pro();
// me.pinfo();
you.pinfo();
you.pro();


