// anything that is outside of a duntion is called as a global space 

var a=10; //this is in the global space
var  x= 10;
console.log("value of x with this in global : " + this.x);
function b(){
    var x= 5; //this is in local scope 
    console.log("value of x with this in the function(local ):: " + this.x);
}
b();
// use of this key word
let laptop={
    cpu : "Ryzen 5",
    ram : 16,
    brand: 'asus',
    getconfig: function (){
        // thisis a anonymos type of function
        // console.log(laptop.cpu);  way 1
        console.log(this.cpu); //other is the use of the the this keyword
        // when we use this keyword in the object that will point to the varibele / properties present in the object
        // if it is a named function then by default it will point to the global value even though
        // it has a local value in it
    }
}
laptop.getconfig();

// now real assignment starts here 
/**
 * suppose you have a function then according to w3 school whnen ever we use this in a named functio n
 * then it point to the window object but its behaviour changes as we use it in inside of an obajedct as shown abouve 
 * but we use a nested named function in a object like below we see that the this which is supposed to refer the object is now 
 * referring to the global object 
 */

// golbol reference 
function doSomething(){
    this.name = "Hrithik";
}
doSomething();
document.write(window.name); // referred to global and executed on browser
// console.log(window.name);

// now what if its in objets method eg
let add = {
    name : "Hrithik",
    num  : 0 ,
    cal: function (){
        this.num +=1 ; 
        console.log(this.name + " "+ this.num);
    }
}
console.log(add.cal()); //object accecessors (two types dots and () )
// however the value of this is set to window by default so if we use strict mode 
// then this value is set to undefined instead of the window which is salos true on other object method declarations
document.write("<br>value of this in the strict mode:")
function doSomething1(){
    'use strict'; //enabling the strict mode;
    document.write(" <br>   this value of this is from doSomething1 function: "+ this + "  <br>"); //this will log undefined in the browser
    function innerf(){
        document.write("    the value of this from the inner function: " + this + " <br><br> ");
        // as yuo can see as we have used the strict mode  the default value of this is now set to undefined

    }
    innerf();
}
doSomething1(); //function invocation

// docuemnt.write("<br> what happens to this in a function nested inside the method of object: ")
let add1 = {
    num :0,
    cal: function(){
        document.write("<br<br>value of this from outer funtion: "+ this +" ");
        // now when we declare a named functino it will point to the global window
        function innerf(){
            this.num +=1;
            document.write("<br>value of this to from the inner function: "+ this + ' ');
            return this.num;

        }
        return innerf();

    }

};
document.write(add1.cal() + '<br>');
// above methods return nan i.e not a number because the the return this.num which is in the function block scope and the inner function
// we are tynig to acccess it through global scope which is not present in it so it gives the not a number
// now this issue is solved by assigning the this value inside the method to a temp variable so that we can have its access even when we declare a named function
// inside the method

let add2= {
    num  :0,
    cal : function(){
        thisref = this;  //this will store the value of the this so that the properties can be accesed in the inner named function

        document.write("<br><br>this value is from another add function add2 to cooreect previous mistake: "+this+ " ");
        function innerf(){
            thisref.num +=1; //incrementing the value using the thisref so that this wont have any role here
            document.write("<br>thisref value: "+thisref +" ");
            return thisref.num 
        }
        return innerf()

    }
};
document.write(add2.cal());

// now insteadof storing the value of this before the named function what we can do is 
// use methods like call,apply,bind which are related to this
//************************************************************************** */

let name11 = {
    firstName: "Hrithik",
    lastName: "Anagire",
    fname : function(){
        console.log(this.firstName +" " + this.lastName);
    }
}
name11.fname();
console.log("this : calling method results: ")
let name2= {
    firstName :"Lucifer",
    lastName :"Morningstar",
    // now one way to print the fullname of this object would be to write the same function property as above , but that not the goo way
    // so we use call function thorugh function borrowing

};

//function borrowing : method of using fuction of a object and use it for data of another object
name11.fname.call(name2); //here we want to acces the properties of name 2 throught call with nme 1 i.e here we will specify the first argument this pointing to object name2 
// each and every method in javascript has  access to a special function  called as call
// in call the first argument will be reference i,.e   which object we want the this varibale to point to that obj name 


// NOTE: that the properties name apart from methods needs to be same any change in property name will result in undefined value
// another way to resue the methods is to decalre it outside as in variable or let and pass the object example


let name33 ={
    firstName : "Monty",
    lastName : "Cassano"
}
let pfname = function(){
    console.log(" with common pfname function outside the object: ");
    console.log(this.firstName +  " " + this.lastName);

}
// and call and passing be like
pfname.call(name33);

// what of ew have more parameters in the function then 

let pfname11 = function(hometown){
    console.log(" with common pfname11  function with parameters outside the object: ");
    console.log(this.firstName +  " " + this.lastName + " from  " + hometown);

}

pfname11.call(name33, "Las Vegas");
// if more parameters then add them with comma separation

//  METHOD 2: APPLY METHOD:
    // only difference between call and apply method is the way we pass the arguments or parameters
    let pfname12 = function(hometown,states){
        console.log(" with common pfname11  function with parameters outside the object: [APPLY METHOD] ");
        console.log(this.firstName +  " " + this.lastName + " from  " + hometown + "," + states);
    
    }
// IN call method we pass the arguments as comma separated but in apply method we pass them as list or array
pfname12.apply(name33,["Las vegas", "US"]);

// bind method looks exactyle same as call method but the only difference is that 
// it binds the function pfname with the object and returns a new a copy of the method
let printMyName = pfname12.bind(name33,"Las Vegas "," US");
// and above printMyName is a function which can be invoked whenever needed
printMyName();
// bind is used to bind the function and object  and keep a copy of it so that it can be used whenever required


/**
 * call : parametes are passed with commas 
 * apply : same as cll but the parameters are passed in single list after passing the pointing object
 * bind : this will return the copy of a function + objecet as a fucntion  that can be used or invoked later
 */
// Function currying:

let multi = function(x,y){
    console.log(x*y);
}
let multB2 = multi.bind(this,2); //this here points to multi function and 2 is set as value for x
multB2(5) ; //this 5 value is now set to y and function will be executed
// in currying we intentionally pass x first and then y


// ananymous function as used when functions are used as values